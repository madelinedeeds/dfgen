/*

  NOTE 10/09/19:
  I'm extending the functionality of this program to work for python package detection
  in a similar way to Jason's implementation. This will require an interface of parsers

*/

#include "dfgen.h"
#include "cparser.h"
#include <iostream>

// NOTE:
// implement multi language support for c/py/r projects in the future
DFGen<CParser> g_c_df{};

int main(int argc, char** argv)
{
  std::vector<std::string> args{};
  std::string in;

  // setup args
  for(char** arg = argv; *arg; ++arg)
  {
    args.push_back(*arg);
  }

  if(args.size() <= 1)
  {
    fprintf(stderr, "[ERR] USAGE: dfgen /path/to/prov.log\n");
    return EXIT_FAILURE;
  }
  else
  {
    // we need to determine which parser to use (or multiple?)
    fprintf(stdout, "[###] for which language would you like to parse?\n");
    fprintf(stdout, "[###] [c(pp)] / [p(ython)]\n[###]>");
tryagain:
    std::getline(std::cin, in);
    if(in.empty())
    {
      goto tryagain;
    }

    switch(in[0])
    {
      case 'c':
      case 'C':
        {
          //g_c_df.set_parser((CParser*)malloc(sizeof(CParser)));
          g_c_df.set_parser(new (std::nothrow)CParser);
          break;
        }
      case 'p':
      case 'P':
        // TODO: support py
        //get_parser() = (IParser*) new (std::nothrow)PYParser; 
        break;
      default:
        goto tryagain;
    }

    // walk through the file and setup our data structures
    fprintf(stdout, "[===] Parsing provenance log\n");
    g_c_df.get_parser()->parse_provlog(fs::path(args[1]));


    // determine which lines give us information about dependencies
    g_c_df.get_parser()->determine_dependencies();  


    // we need to print these to get an idea of the precision/recall
    fprintf(stdout, "[###] User-Shared libraries:\n");
    for(auto& dep : g_c_df.get_parser()->get_deps())
    {
      fs::path p = dep.get_line().m_arglist[0];

      fprintf(stdout, " %s\n", p.filename().generic_string().c_str());
    }

    // setup the path for the dockerfile
    g_c_df.get_parser()->init_root_path();

    // make the dockerfile!
    g_c_df.setup_dockerfile();
  }

  // we succeeded. yay.
  return EXIT_SUCCESS;
}
