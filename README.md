Theoretically, you could engineer this project to automatically generate all of this. However, it requires further engineering.

```
# information received from: 
# https://zenodo.org/record/3379611
# MAINTAINER georgios.bitzes@cern.ch

# this is assumed for now.
FROM ubuntu

# build-essential: contains gcc/g++ compilers
#
# dependendencies identified from:
# libssl1.1 ------> libssl.so.1.1
# libxml2 --------> libxml.so.2
#
RUN apt-get update \
	&& apt-get install -y build-essential \
	&& apt-get install -y libssl1.1 \
	&& apt-get install -y libxml2 

# project name is automatically determined from the parent directory
WORKDIR /davix

# places the executed binary and relative dependency in the working directory '/davix'
ADD davix-tester libdavix.so.0 ./

# executes the original program with the same arguments
CMD ["./davix-tester", "--help"]
```
