#pragma once

#include <unistd.h>

#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <utility>
#include <sstream>
#include <cstring>
#include <algorithm>
#include <functional>

#include <experimental/filesystem>
namespace fs = std::experimental::filesystem;

// every type of syscall spewed by prov log
enum syscall_type_t
{
  syscall_undefined = -1,
  EXECVE,
  EXECVE2,
  MEM,
  READ,
  WRITE,
  READWRITE,
  UNKOWNIO,
  SPAWN,
  CLOSE,
  OPEN,
  LEXIT,
  EXIT,
};

// unk command, e.g. '_x_ _y_'
using unk_cmd_list_t = std::vector<std::pair<std::string, std::string>>;

// read/closed pairs of commands
// TODO: implement "OPEN" log 
using rc_pair_t = std::pair<uint32_t, std::string>;

// known command, e.g. 'RUN _x_'
using cmd_list_t = std::vector<std::string>;

// this is a raw line in the prov log
struct line_data_t
{
    uint32_t m_timestamp,
            m_pid;
    syscall_type_t m_syscall;
    cmd_list_t m_arglist;
};

// types of dependencies
// feel free to implement and add to this
enum dep_type_t 
{
  _undefined_dep = -1,
  CCPP, // c or cpp dependency (.so user-shared library)
  PY    // python dependency 
};

// we need a dependency interface
class IDependency
{
public:

  IDependency(){}
  ~IDependency(){}

  virtual dep_type_t get_type() = 0;
  virtual bool is_nested_dep() = 0;

  // smart pointers would be nice here but i can't be bothered
  virtual IDependency* get_parent_dep() = 0;
  virtual IDependency* get_child_dep() = 0;

  auto& get_line()
  {
    return m_line;
  }

  /*bool operator==(const IDependency& a)
  {
    // this is cheap, try n stop me :+)
    // return (memcmp(&m_line, &a.m_line, sizeof(line_data_t)) == 0);

    // this is also cheap
    return (m_line.m_arglist[0].compare(a.m_line.m_arglist[0]) == 0);
  }*/

  bool has_same_arg(const line_data_t& cmp, size_t which = 0)
  {
    return (m_line.m_arglist[which].compare(cmp.m_arglist[which]) == 0);
  }

private:
  line_data_t m_line;
};

// path rule callback
using path_rule_callback_t = std::function<bool(const std::string&)>;

// special paths have special rules
//
// std::string ----------> absolute path
// path_rule_callback_t -> callback
//
using special_path_rule_t = std::pair<std::string, path_rule_callback_t>;

// parser interface
//
// the way this works: if you want to implement your own parser on top
// of this all you need to do is create your own impl. e.g.
//
// `class ExampleParser : public IParser`
// 
// from there, you need to plug in the required methods for the parser
// and implement various methods you need to determine what is/isn't a 
// valid dependency
// 
class IParser
{
public:
    // for each type of parser, there will be specific paths to filter out
    virtual std::vector<std::string> get_bad_paths() = 0;

    // conversely, there may be *good* paths?
    virtual std::vector<std::string> get_good_paths() = 0;

    // special paths have special rules and thusly...
    // special callbacks
    virtual std::vector<special_path_rule_t> get_special_paths() = 0;

    // in each parser we need to determine if a line is 
    // going to give us a dependency
    virtual bool is_good_dependency_line(const line_data_t& ld) = 0;

    // need a big funco to setup the dependency vector
    virtual void determine_dependencies() = 0;

    // we need a way of checking what type of syscall is in a line
    // TODO:
    // this is nasty, maybe some djb2 hash comparisons would be a lot faster
    syscall_type_t typeof_syscall(const std::string& line)
    {
      auto matches_str = [](const std::string& str, const std::string& item){
        return (str.compare(item) == 0);
      };

      if (matches_str(line, "EXECVE"))
        return syscall_type_t::EXECVE;
      if (matches_str(line, "EXECVE2"))
        return syscall_type_t::EXECVE2;
      if (matches_str(line, "MEM"))
        return syscall_type_t::MEM;
      if (matches_str(line, "READ"))
        return syscall_type_t::READ;
      if (matches_str(line, "WRITE"))
        return syscall_type_t::WRITE;
      if (matches_str(line, "READ-WRITE"))
        return syscall_type_t::READWRITE;
      if (matches_str(line, "UNKOWNIO"))
        return syscall_type_t::UNKOWNIO;
      if (matches_str(line, "SPAWN"))
        return syscall_type_t::SPAWN;
      if (matches_str(line, "LEXIT"))
        return syscall_type_t::LEXIT;
      if (matches_str(line, "CLOSE"))
        return syscall_type_t::CLOSE;
      if (matches_str(line, "OPEN"))
        return syscall_type_t::OPEN;
      if (matches_str(line, "EXIT"))
        return syscall_type_t::EXIT;

      return syscall_type_t::syscall_undefined;
    }

    // this is a big function, and i don't like to put it directly in the class but 
    // it needs to be here
    //
    // all this does is iterate through every line in the prov log and store
    // the data that is present in them
    //
    void parse_provlog(const fs::path& path)
    {
        std::ifstream file{path};

        cmd_list_t cmds{};
        line_data_t ld{};

        for(std::string line; std::getline(file, line);)
        {
          if(line.empty())
            continue;

          // skip descriptive/comment lines
          if(line[0] == '#')
            continue;

          // make sure the beginning of the line starts with a digit (timestamp)
          if(!std::isdigit(line[0]))
            continue;

          // clear our previous line cmds and line data
          cmds.clear();
          memset(&ld, 0, sizeof(line_data_t));

          // split the line into multiple strings
          std::istringstream tmp(line);
          for(std::string s; tmp >> s;)
          {
            cmds.push_back(s);
          }

          // grab timestamp
          ld.m_timestamp = std::atoi(cmds[0].c_str());

          // grab pid
          ld.m_pid = std::atoi(cmds[1].c_str());

          // grab typeof syscall
          ld.m_syscall = typeof_syscall(cmds[2]);

          // check that we even read a correct syscall:
          if(ld.m_syscall == syscall_type_t::syscall_undefined)
          {
            std::string in;

            fprintf(stderr, "[ERR] syscall of type %s is undefined\n", cmds[2].c_str());
tryagain:
            fprintf(stderr, "[ERR] continue? [y/n]\n[ERR]>");

            std::getline(std::cin, in);
            if(in.empty())
            {
              goto tryagain;
            }

            switch(in[0])
            {
              case 'y':
              case 'Y':
                continue;
                break;
              case 'n':
              case 'N':
                return;
                break;
              default:
                goto tryagain;
            }
          }

          // store rest of line
          for(int i=3; i < cmds.size(); ++i)
          {
             ld.m_arglist.push_back(cmds[i]);
          }
          m_lines.push_back(ld);
        }
    }

    void init_root_path()
    {
      for(auto& ld : m_lines)
      {
        // the first binary to be executed is typically going to be the "root" path
        if(ld.m_syscall == syscall_type_t::EXECVE)
        {
          // this again, ugh
          auto strpath = ld.m_arglist[3];
          
          // preserve this!
          m_exec = strpath;

          strpath.erase(std::remove(strpath.begin(), strpath.end(), '['), strpath.end());
          strpath.erase(std::remove(strpath.begin(), strpath.end(), ']'), strpath.end());
          strpath.erase(std::remove(strpath.begin(), strpath.end(), '"'), strpath.end());

          auto end = strpath.find_last_of('/');
          if(end != std::string::npos)
          {
            if(strpath[end - 1] == '/' && strpath[end - 2] == '.')
            {  
              end -= 2;
            } else
            {
              end += 1;
            }
          }
          m_root_bin_name = strpath.substr(end);

          //fprintf(stdout, "[+++] tetest: %s\n", strpath2.c_str());

          auto path = fs::path(strpath);

          if(path.has_filename())
          {
            m_proj_name = path.filename();
          } else
          {
            m_proj_name = path.parent_path();
          }

          fprintf(stdout, "[+++] proj name: %s\n", m_proj_name.c_str());
          fprintf(stdout, "[+++] proj filename: %s\n", m_root_bin_name.c_str());

          // this line will incl the binary's name, so remove it
          m_root_path = fs::path(ld.m_arglist[1]).parent_path();

          fprintf(stdout, "[+++] proj path: %s\n", m_root_path.c_str());

          return;
        }
      }
    }

    auto& get_root_path()
    {
      return m_root_path;
    }

    auto& get_root_filename()
    {
      return m_root_bin_name;
    }

    auto& get_execution()
    {
      return m_exec;
    }

    // basic getters
    auto& get_lines()
    {
       return m_lines;
    }

    auto& get_proj_name()
    {
      return m_proj_name;
    }

private:
    fs::path m_root_path;
    std::string m_root_bin_name;
    std::string m_proj_name;
    std::string m_exec;
    std::vector<line_data_t> m_lines;
};

