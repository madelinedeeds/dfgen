#pragma once

#include "iparser.h"

class CDependency : public IDependency
{
public:
    dep_type_t get_type()
    {
        return dep_type_t::CCPP;
    }

    bool is_nested_dep()
    {
        return false;
    }

    // TODO: smart pointers 
    IDependency* get_parent_dep(){return nullptr;};
    IDependency* get_child_dep(){return nullptr;};

    auto& get_line()
    {
        return m_line;
    }

    void set_line(const line_data_t& ld)
    {
        m_line = ld;
    }

private:
    line_data_t m_line;
};

class CParser : public IParser
{
public:
    CParser(){}
    ~CParser(){}

    // filter out system libraries here:
    // this is *CRICAL*
    // i don't know a proper/better way to determine if a dynlib belongs to the system
    // perhaps read the ELF header could give us some information
    //
    // bad_paths:
    // "x86_64-linugnu" <-- system specific
    // "etc"            <-- don't know if this is bad yet, but it threw sys libs
    std::vector<std::string> get_bad_paths()
    {
        return 
        {
            "/libgtk3-nocsd", // this is a part of LD_PRELOAD-
            "/etc/" // ?
        };
    }

    std::vector<std::string> get_good_paths()
    {
        return 
        {
            "/usr/local/lib/"
        };
    }

    std::vector<special_path_rule_t> get_special_paths()
    {
        return
        {           
            special_path_rule_t
            { 
                // we need to determine if this is:
                // 
                // /usr/lib <- seems to be user installed libraries
                // ...or... 
                // /lib/    <- seems to be _mostly_ kernel libraries
                //
                "/lib/x86_64-linux-gnu/", 
                [](const std::string& abspath) -> bool
                {
                    fs::path p(abspath);              
                    fs::path first_dir;
                    //if the root directory of the path is /lib/, then we know we can skip these

                    for(fs::path pp = p; pp.has_parent_path(); pp = pp.parent_path())
                    {   
                        // this will be constantly overwritten until it hits the last one :)
                        // could be more optimized or done more properly but i cant think of anything better
                        first_dir = pp;
                    }

                    if(first_dir.string().compare("/lib") == 0)
                    {
                        // kernel library. abort mission?
                        // i mean, according to the provenance log this *was* accessed.
                        // i'm very curious about how one would reproduce effectively without certainl kernel libraries...
                        // there are some very simple ones present on minimal linux distributions-- 
                        // but every distro is different.
                        //
                        // NOTE: find more unique ways of identifying system libraries
                        //
                        return false; 
                    } else
                    {
                        return true;
                    }   
                }
            },

        };
    }

    // in each parser we need to determine if a line is 
    // going to give us a dependency
    bool is_good_dependency_line(const line_data_t& ld)
    {
        // TODO:
        // check for [OPEN]/READ/CLOSE
        if(ld.m_syscall != syscall_type_t::READ)
            return false;

        std::string p = ld.m_arglist[0];

        fs::path abspath(p);
        fs::path filename = abspath.filename();

        // sanity check the filename
        if(filename.empty())
            return false;

        // firstly, drop all the stuff we're certain is not going to lead us anywhere
        for(auto badpath : get_bad_paths())
        {
            // skip bad paths
            if(p.find(badpath)!=std::string::npos)
                return false;
        }

        // just return if its a good path
        for(auto goodpath : get_good_paths())
        {
            if(p.find(goodpath) != std::string::npos)
            {
                return (filename.string().find(".so") != std::string::npos);
            }
        }

        // if its not a bad path, then we 
        // need to check for special paths, 
        // which invoke a callback with the path as a parameter
        for(auto specialpath : get_special_paths())
        {
            if(p.find(specialpath.first) != std::string::npos)
            {
                // run against the callback 
                if (!specialpath.second(p))
                    return false;
            }
        }

        // this will always be present in dynamic linux libraries
        return (filename.string().find(".so") != std::string::npos);
    }


    // this exists in each impl but i cant interface it right now because im not enough of a cpp wizard
    auto& get_deps() 
    {
        return m_deps;
    }

    // need a big funco to setup the dependency vector
    void determine_dependencies()
    { 
        for(auto& ld : get_lines())
        {
            if(is_good_dependency_line(ld))
            {
                CDependency dep{};

                dep.set_line(ld);

                get_deps().push_back(dep);
            }
        }       
    }  

private:
    std::vector<CDependency> m_deps;
};
