#pragma once

// all the parsers should be incl here
#include "cparser.h"

template <typename _Parser>
class DFGen
{
public:
  DFGen(){}
  ~DFGen(){}

  // saves me typing
  using DFGen_t = DFGen<_Parser>;

  bool find_binary_for_lib(uint32_t pid, std::string& pathout, std::string& filenameout)
  {
    for(auto& ld : get_parser()->get_lines())
    {
      if(ld.m_syscall == syscall_type_t::EXECVE)
      {
        // grab that pid from the arg list
        uint32_t syscall_pid = std::atoi(ld.m_arglist[0].c_str());
        if(syscall_pid = pid)
        {
          // create a full, absolute path to the binary
          std::string dir = ld.m_arglist[2].append("/");
          std::string filename = ld.m_arglist[3];

          // cleanup the filename
          filename.erase(std::remove(filename.begin(), filename.end(), '['), filename.end());
          filename.erase(std::remove(filename.begin(), filename.end(), ']'), filename.end());
          filename.erase(std::remove(filename.begin(), filename.end(), '"'), filename.end());

          // presrve the filename
          filenameout = filename;

          // we're pretty sure this is the one opening the
          pathout = dir;

          return true;
        }
      }
    }

    return false;
  }

  // another big funco
  // NEEDS A LOT OF WORK.
  // TODO: ...
  void setup_dockerfile()
  {
    std::string dir;
    std::string filename;
    std::string rpath;

    std::vector<std::pair<std::string, std::string>> bins_to_exec;

    // need to remove the dumb quotes in the path
    rpath = get_parser()->get_root_path();
    rpath.erase(std::remove(rpath.begin(), rpath.end(), '"'), rpath.end());

    // make the dockerfile
    std::stringstream dir_to_make;
    dir_to_make << rpath << "/" << "Dockerfile";

    m_dockerfile.open(dir_to_make.str().c_str(), std::ios::out);

    auto res = std::time(nullptr);
    m_dockerfile << "# DFGen created Dockerfile\n";
    m_dockerfile << "# created at " << res << ":" << std::asctime(std::localtime(&res)) << std::endl;

    // setup all the commands
    m_dockerfile << "FROM ubuntu\n\n";

    m_dockerfile << "RUN apt-get update && apt-get install -y build-essential\n\n";

    
    // set the workdir
    m_dockerfile << "WORKDIR /" << m_parser->get_proj_name() << "\n\n";

    // every file needs to be copied over. 
    // this might get complex and ugly, docker does not support abspath copies
    // copy everything over
    for(auto& s : get_parser()->get_deps())
    {
      std::stringstream cmd;

      auto ld = s.get_line();

      cmd << "sudo cp " << ld.m_arglist[0] << " " << rpath << "\n";

      system(cmd.str().c_str());
    }

    // copy the binary
    m_dockerfile << "COPY " << m_parser->get_proj_name() << " ";

    // copy everything over
    for(auto& s : get_parser()->get_deps())
    {
      auto ld = s.get_line();
      auto fn = fs::path(ld.m_arglist[0]).filename();

      // list out all the files to copy over
      m_dockerfile << fn.string() << " ";
    }

    // finish the copy command by making sure everything copies over to 
    // our container
    m_dockerfile << " ./\n\n";

    // add the root execution, then CMD it!
    m_dockerfile << "CMD [\"" << get_parser()->get_root_filename() << "\"]\n";

    m_dockerfile.close();
  }

  // templated parser getter
  auto& get_parser()
  {
    return m_parser;
  }

  void set_parser(_Parser* p)
  {
    m_parser = p;
  }

private:
  _Parser* m_parser;

  std::fstream m_dockerfile;
  cmd_list_t m_args;
};

// c/cpp
class CParser;
extern DFGen<CParser> g_c_df;
