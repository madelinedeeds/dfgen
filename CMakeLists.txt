cmake_minimum_required (VERSION 3.0)

include_directories(.)

set(CMAKE_BUILD_TYPE Debug)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++17 -lstdc++fs")
set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -Wall")

project (dfgen)

link_libraries(stdc++fs)

add_executable(
	dfgen
	main.cpp
)

